import './MainContent.css'

function MainContent() {
    return (
        <main className="main">
            <h1 className="heading1">Fun Facts about React</h1>
            <ul className='ulContainer'>
                <li className='li'>Was first released in 2013</li>
                <li className='li'>Was originally created by Jordan Walke</li>
                <li className='li'>Has well over 100K stars in Github</li>
                <li className='li'>Is maintained by Facebook</li>
                <li className='li'>Powers thousands of enterprise apps, including mobile apps</li>
            </ul>
        </main>
    )
}

export { MainContent }