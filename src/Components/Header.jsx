import logo from '../assets/react.svg'
import './Header.css'

function Header(){
    return (
        <header className='header'>
            <nav className='nav'>
                <img src={logo} alt="react-logo" className='react-logo'/>
                <h3 className='react-logo-side-heading3'>ReactFacts</h3>
                <h4 className='react-logo-side-heading4'>React Course-Project-1</h4>
            </nav>
        </header>
    )
}

export {Header}