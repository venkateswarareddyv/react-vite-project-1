import React from 'react'
import ReactDOM from 'react-dom/client'
import {Header}  from "./Components/Header";
import  {MainContent}  from "./Components/Maincontent";

ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
      <Header />
      <MainContent/>
    </React.StrictMode>,
  )
  